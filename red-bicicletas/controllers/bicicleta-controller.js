var Bicicleta = require('../models/bicicleta');

// LISTAR
exports.bicicleta_list = function(req, res){
    res.render('bicicletas/bicicleta-view', {bicicletas: Bicicleta.allBicis});
}

// CREAR
exports.bicicleta_create_get = function(req, res){
    res.render('bicicletas/bicicleta-form');
}

exports.bicicleta_create_post = function(req, res){
    var bicicleta = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bicicleta.ubicacion = [req.body.latitud, req.body.longitud];
    Bicicleta.add(bicicleta);
    res.redirect('/bicicletas');
}

// MODIFICAR
exports.bicicleta_update_get = function(req, res){
    var bici = Bicicleta.findById(req.params.id);
    res.render('bicicletas/bicicleta-form2', {bici});
}

exports.bicicleta_update_post = function(req, res){
    var bici = Bicicleta.findById(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.latitud, req.body.longitud];
    res.redirect('/bicicletas');
}

// ELIMINAR
exports.bicicleta_delete = function(req, res){
    Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');
}