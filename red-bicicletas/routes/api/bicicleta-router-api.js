var express = require('express');
var router = express.Router();
var bicicletaController = require('../../controllers/api/bicicleta-controller-api');

// GET
router.get('/', bicicletaController.bicicleta_list);

// POST
router.post('/crear', bicicletaController.bicicleta_create);

router.delete('/eliminar', bicicletaController.bicicleta_delete);

module.exports = router;