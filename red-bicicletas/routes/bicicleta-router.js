var express = require('express');
var router = express.Router();
var bicicletaController = require('../controllers/bicicleta-controller');

// /GET
router.get('/', bicicletaController.bicicleta_list)

// POST
router.get('/crear', bicicletaController.bicicleta_create_get);
router.post('/crear', bicicletaController.bicicleta_create_post);

// UT
router.get('/:id/editar', bicicletaController.bicicleta_update_get);
router.post('/:id/editar', bicicletaController.bicicleta_update_post);

// DELETE
router.post('/:id/borrar', bicicletaController.bicicleta_delete);

module.exports = router;